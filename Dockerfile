FROM ubuntu:focal
MAINTAINER Lukas Mertens <docker@lukas-mertens.de>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -qy \
    ansible \
    make git build-essential ssh-client rsync \
    && rm -rf /var/lib/apt/lists/*
RUN ansible-galaxy collection install community.general community.docker

WORKDIR /data
VOLUME ["/data"]
